<?php

    $res = array(
        "x" => 0,
        "y" => 0,
        "r" => 0,
        "time" => microtime(true),
        "delay" => 0,
        "isHit" => false,
        "status" => false,
        "msg" => ""
    );

    if ((validateX($_GET["x"])) && (validateY($_GET["y"])) && (validateR($_GET["r"]))) {
        $res["status"] = true;
        $res["x"] = (float)str_replace(",", ".", $_GET["x"]);
        $res["y"] = (float)str_replace(",", ".", $_GET["y"]);
        $res["r"] = (float)str_replace(",", ".", $_GET["r"]);
        $res["isHit"] = checkDot($res["x"], $res["y"], $res["r"]);
    } else {
        $res["msg"] = "IncorrectValue";
    }
    $res["delay"] = (microtime(true) - $res["time"]) * 1000;

    echo json_encode($res);

//     echo "{\"x\": 111, \"y\": 222, \"r\": 333, \"time\": 1.222, \"delay\": 0.2, \"status\": \"hit\"}";



    function validateNumber($number, $min, $max) {
        $number = (float)str_replace(",", ".", $number);
        if (is_numeric($number) && $number >= $min && $number <= $max) {
            return true;
        }
        return false;
    }

    function validateX($value) {
         return (is_numeric($value) && is_int($value + 0) && $value >= -5 && $value <= 3);
    }

    function validateY($value) {
        return ($value >= -3 && $value <= 5);
    }

    function validateR($r) {
        $arr = array("1", "1.5", "2", "2.5", "3");
        return in_array($r, $arr);
    }

    function checkDot($x, $y, $r) {
        if($x < 0) {
            if($y >= 0) {
                return sqrt($x * $x + $y * $y) <= $r;
            }
            return abs($x) + abs($y) <= ($r / 2);
        } else {
            if($y >= 0) {
                return $x <= $r / 2 && $y <= $r;
            }
            return false;
        }
    }
?>
